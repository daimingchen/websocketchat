package com.example.webchat.constant;

public class JwtClaimsConstant {
    public static final String USER_NAME = "username";
    public static final String USER_ID = "sid";
}
