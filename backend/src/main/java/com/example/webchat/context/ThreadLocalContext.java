package com.example.webchat.context;

import java.util.Map;

public class ThreadLocalContext {

    private static ThreadLocal<Map> webSocketThreadLocal = new ThreadLocal();

    public static void setWebsocketUserInfo(String username, String sid) {
        Map<String, String> sid2username = webSocketThreadLocal.get();
        sid2username.put(sid, username);
        webSocketThreadLocal.set(sid2username);
    }

    public static String getWebsocketUsernameBySid(String sid) {
        Map<String, String> sid2username = webSocketThreadLocal.get();
        String username = sid2username.get(sid);
        return username;
    }

    public static void removeWebsocketUserBySid(String sid) {
        Map<String, String> sid2username = webSocketThreadLocal.get();
        sid2username.remove(sid);
        webSocketThreadLocal.set(sid2username);
    }
}
