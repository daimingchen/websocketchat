package com.example.webchat.entity;


import com.example.webchat.constant.ResultConstant;
import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {

    private Integer code; //编码：1成功，0和其它数字为失败
    private String msg; //错误信息
    private T data; //数据

    /**
     * 携带数据返回前端
     * @param data
     * @return
     */
    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<T>();
        result.setCode(ResultConstant.OK);
        result.setMsg(ResultConstant.MSG_OK);
        result.setData(data);
        return result;
    }

    public static Result success() {
        Result result = new Result();
        result.setCode(ResultConstant.OK);
        result.setMsg(ResultConstant.MSG_OK);
        return result;
    }


    public static <T> Result<T> error(Integer code, String errMsg) {
        Result<T> result = new Result<T>();
        result.setCode(code);
        result.setMsg(errMsg);
        return result;
    }
}
