import Vue from 'vue'
import axios from 'axios'  // 引入axios

const service = axios.create({
    baseURL: process.env.API_BASE_URL, // 公共请求地址
    timeout: 6000 // 请求超时时间
})

// 请求拦截器
service.interceptors.request.use((config) => {
    // Vue.ls本地储存token
    const token = Vue.ls.get('ACCESS-TOKEN')
    // const locale = Vue.ls.get(LANG)
    if (token) {
        // 添加token请求头
        config.headers['Authorization'] = token
    }
    // config.headers.Locale = locale
    return config
}, error => Promise.reject(error))

// 响应拦截器
service.interceptors.response.use(
    (error) => {
        // 响应异常处理
        // Message.error('Network Error')
        return Promise.reject(error)
    }
)

// as关键字设置别名
export { service as axios } 