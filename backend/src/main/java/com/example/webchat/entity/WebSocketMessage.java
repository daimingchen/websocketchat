package com.example.webchat.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class WebSocketMessage implements Serializable {
    String username;
    String msg;
}
