package com.example.webchat.service;

public interface UserService {
    /**
     * 用户登录，返回JWT令牌，在24小时内有效
     * @param username
     * @return
     */
    String login(String username);
}
