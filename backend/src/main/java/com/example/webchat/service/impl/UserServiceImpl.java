package com.example.webchat.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.webchat.constant.JwtClaimsConstant;
import com.example.webchat.entity.JwtProperties;
import com.example.webchat.service.UserService;
import com.example.webchat.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 用户登录，返回JWT令牌，24小时内有效
     * @param username
     * @return
     */
    public String login(String username) {
        String sid = getRandomSid(12);
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.USER_NAME, username);
        claims.put(JwtClaimsConstant.USER_ID, sid);
        String token = JwtUtil.createJWT(
                jwtProperties.getUserSecretKey(),
                jwtProperties.getUserTtl(),
                claims);

        return token;
    }

    private String getRandomSid(int size) {
        Random r = new Random();
        StringBuilder sid = new StringBuilder();
        final String dictionary = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 0; i < size; i++) {
            int index = r.nextInt(dictionary.length());
            sid.append(dictionary.charAt(index));
        }
        return sid.toString();
    }
}
