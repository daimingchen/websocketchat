package com.example.webchat.controller;

import com.example.webchat.entity.Result;
import com.example.webchat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 用户登录
     * @param username
     * @return
            GET http://localhost:8080/login?username=Tom
     */
    @GetMapping("/login")
    public Result login(String username ) {
        String jwtToken = userService.login(username);
        return Result.success(jwtToken);
    }

    @GetMapping("/test")
    public Result test() {
        return Result.success();
    }
}
