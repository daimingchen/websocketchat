package com.example.webchat.interceptor;

import com.example.webchat.constant.JwtClaimsConstant;
import com.example.webchat.context.ThreadLocalContext;
import com.example.webchat.entity.JwtProperties;
import com.example.webchat.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * jwt令牌校验的拦截器
 */
@Component
@Slf4j
public class JwtTokenInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 校验jwt
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //判断当前拦截到的是Controller的方法还是其他资源
        if (!(handler instanceof HandlerMethod)) {
            //当前拦截到的不是动态方法，直接放行
            return true;
        }

        //1、从请求头中获取令牌
        String token = request.getHeader(jwtProperties.getUserTokenName());

        //2、校验令牌
        try {
            log.info("jwt校验:{}", token);
            Claims claims = JwtUtil.parseJWT(jwtProperties.getUserSecretKey(), token);
            String username = claims.get(JwtClaimsConstant.USER_NAME, String.class);
            String sid = claims.get(JwtClaimsConstant.USER_ID, String.class);
            ThreadLocalContext.setWebsocketUserInfo(username, sid);
            log.info("当前登录用户为 {}, WebSocket ID = {}", username, sid);
            //3、通过，放行
            return true;
        } catch (Exception ex) {
            //4、不通过，响应401状态码，让前端重定向到登陆页面
            response.setStatus(401);
            return false;
        }
    }
}
