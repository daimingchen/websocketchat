package com.example.webchat.handler;

import com.example.webchat.constant.ResultConstant;
import com.example.webchat.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理器，处理项目中抛出的业务异常
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 捕获业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler
    public Result exceptionHandler(Exception ex){
        String  errMsg = "业务异常，请查看服务器日志：" + ex.getMessage();
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(ResultConstant.ERROR, errMsg);
    }
}
